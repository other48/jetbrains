/*
	PHPStorm, JavaScript CodeStyle
*/

/* 1. Нет настройки для переноса открывающейся фигурной скобки на новую строку при форматировании объектов. */

// Исходный код:
var object = {
	name: 'Ivan',
	age: 24,
	place_of_residence: {
		country: 'Russia',
		city: 'Saint-Petersburg'
	}
};

// Ожидаемый результат:
var object =
{
	name : 'Ivan',
	age  : 24,
	place_of_residence :
	{
		country : 'Russia',
		city    : 'Saint-Petersburg'
	}
};



/* 2. Нет настройки "break на том же уровне, что и case" */

// Исходный код:
switch (number)
{
	case 1:
		console.log(number);
		break;
	case 2:
		// ...
		break;
	default:
		console.error('unknown number');
		break;
}

// Ожидаемый результат:
switch (number)
{
	case 1:
		console.log(number);
	break;
	case 2:
		// ...
	break;
	default:
		console.error('unknown number');
	break;
}



/* 3. Перенос скобки при передаче аргумента-объекта */

// Исходный код:
some_method({
	id: 1,
	name: 'Admin'
});
another_method({ id:1, name:'Admin'});

// Ожидаемый результат:
some_method(
{
	id   : 1,
	name : 'Admin'
});
another_method(
{ 
	id   : 1,
	name : 'Admin'
});